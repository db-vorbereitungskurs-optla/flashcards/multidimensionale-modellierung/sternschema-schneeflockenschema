import { useState } from 'react'

import Navbar from './components/Navbar'
import NavigationButtons from './components/NavigationButtons'
import Quizcard from './components/Quizcard'

import { questions } from './questions/questions'

const flashCardTopic = 'Sternschema und Schneeflockenschema'

function App() {
  const [currentlyDisplayedQuestion, setCurrentlyDisplayedQuestion] =
    useState(0)

  function generateQuizCards() {
    return questions.map((question, index) => {
      if (currentlyDisplayedQuestion === index)
        return (
          <Quizcard
            key={'question' + index}
            questionNumber={index + 1}
            questionsTotal={questions.length}
            questionText={question[0]}
            answerText={question[1]}
          ></Quizcard>
        )
    })
  }

  return (
    <>
      <Navbar headline={flashCardTopic}></Navbar>
      <div
        className="d-flex justify-content-center"
        style={{ margin: '1em', alignItems: 'flex-start', gap: '1em' }}
      >
        <NavigationButtons
          maxQuestionIndex={questions.length - 1}
          navigationState={currentlyDisplayedQuestion}
          navigationCallbackFunction={setCurrentlyDisplayedQuestion}
        ></NavigationButtons>
        {generateQuizCards()}
      </div>
    </>
  )
}

export default App
