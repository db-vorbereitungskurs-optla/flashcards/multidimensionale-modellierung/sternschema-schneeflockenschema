export const questions = [
  [
    'Welches Hauptziel verfolgen das Sternschema und das Schneeflockenschema bei der Modellierung relationaler Datenbanken im Kontext der Auswertung und Analyse großer Mengen historischer Daten im Vergleich zu klassischen relationalen Datenbankmodellen?',
    'Die Hauptzielsetzung des Sternschemas und des Schneeflockenschemas liegt nicht primär in der Normalisierung, sondern in der gezielten Optimierung für effiziente Leseoperationen, insbesondere bei der Auswertung und Analyse großer Mengen historischer Daten.',
  ],
  [
    'Wie unterscheiden sich das Sternschema und das Schneeflockenschema hinsichtlich der Hierarchisierung ihrer Dimensionen und welche Auswirkungen hat dies auf die Datenstruktur?',
    'Im Sternschema steht jede Dimensionstabelle in einer direkten 1:N-Beziehung zur zentralen Faktentabelle, wodurch keine hierarchische Unterteilung der Dimensionen möglich ist. Im Gegensatz dazu erlaubt das Schneeflockenschema eine solche Unterteilung, führt jedoch zu einer höheren Normalisierung der Datenstruktur.',
  ],
  [
    'Welche Kompromisse werden beim Einsatz des Sternschemas eingegangen, insbesondere im Hinblick auf die 3. Normalform und welche Vorteile werden dadurch erzielt?',
    'Im Sternschema sind Dimensionstabellen häufig nicht in der 3. Normalform, da sie in direkten 1:N-Beziehungen zur Faktentabelle stehen. Die Kompromisse in Form von geringerer Datenintegrität und höherem Speicherplatzbedarf werden eingegangen, um die Abfrageleistung durch weniger erforderliche Joins zu verbessern, was besonders bei großen Datenmengen von Vorteil ist.',
  ],
]
